# README
Depuis le dossier racine:
- bundle install
- rails db:migrate
- rails db:seed
- rails serve

Depuis le dossier frontend:
- npm install
- ember server --proxy http://localhost:3000

pour accéder à l'application:
http://localhost:4200/features