Rails.application.routes.draw do
  resources :statuses
  # mount_ember_app :frontend, to: '/'
  resources :featuretests
  resources :features
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
