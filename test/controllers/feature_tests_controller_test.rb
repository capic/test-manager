require 'test_helper'

class FeatureTestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @featuretest = featuretests(:one)
  end

  test "should get index" do
    get featuretests_url, as: :json
    assert_response :success
  end

  test "should create featuretest" do
    assert_difference('Featuretest.count') do
      post featuretests_url, params: { feature_test: { feature_id: @featuretest.feature_id, name: @featuretest.name, status: @featuretest.status } }, as: :json
    end

    assert_response 201
  end

  test "should show featuretest" do
    get featuretest_url(@featuretest), as: :json
    assert_response :success
  end

  test "should update featuretest" do
    patch featuretest_url(@featuretest), params: { feature_test: { feature_id: @featuretest.feature_id, name: @featuretest.name, status: @featuretest.status } }, as: :json
    assert_response 200
  end

  test "should destroy featuretest" do
    assert_difference('Featuretest.count', -1) do
      delete featuretest_url(@featuretest), as: :json
    end

    assert_response 204
  end
end
