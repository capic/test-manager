import { test } from 'qunit';
import moduleForAcceptance from 'frontend/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | features');

test('visiting /features', function(assert) {
  let features = server.createList('feature', 10);
  visit('/features');

  andThen(function() {
    assert.equal(currentURL(), '/features');
    assert.equal(find('.test-feature-name').length, 10);
    assert.equal(find('.test-feature-name:first').text(), features[0].name);
  });
});

test('go to feature', function(assert) {
  let features = server.createList('feature', 10);
  visit('/features');

  andThen(function() {
    assert.equal(currentURL(), '/features');
  });

  click('.test-linkto-feature:first');

  andThen(function() {
    assert.equal(currentURL(), '/features/1');
    assert.equal(find('.test-feature-name').text(), features[0].name);
  });
});

test('go to feature creation', function(assert) {
  visit('/features');

  andThen(function() {
    assert.equal(currentURL(), '/features');
  });

  click('.test-button-creation-feature');

  andThen(function() {
    assert.equal(currentURL(), '/features/create');
  });
});
