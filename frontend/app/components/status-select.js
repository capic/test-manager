import Ember from 'ember';

export default Ember.Component.extend({
  notify: Ember.inject.service(),
  store: Ember.inject.service(),
  actions: {
    updateStatus(featuretest, statusId)  {
      const store = this.get('store');
      const notify = this.get('notify');

      store.findRecord('status', statusId).then(function(status){
        featuretest.set('status', status);
        featuretest.save().then(() => {
          notify.success('Feature test ' + featuretest.get('name') + ' Updated!');
        }).catch((reason) => {
          notify.error('Error: ' + reason);
        });
      });
    }
  }
});
