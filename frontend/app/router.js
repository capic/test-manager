import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('features', {path: '/features'});
  this.route('feature', {path: '/features/:feature_id'}/*, function() {
    this.route('statuses');
  }*/);
  this.route('create-feature', {path: '/features/create'});
  this.route('create-featuretest', {path: '/features/:feature_id/create'});
  this.route('statuses');
});

export default Router;
