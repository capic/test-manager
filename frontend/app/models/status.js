import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  featuretests: DS.hasMany('featuretests')
});
