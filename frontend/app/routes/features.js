import Ember from 'ember';

export default Ember.Route.extend({
  notify: Ember.inject.service(),
  model() {
    const notify = this.get('notify');

    return this.store.findAll('feature')
      .catch((reason) => {
        notify.error('Error: ' + reason);
      });
  },
  actions: {
    create() {
      this.transitionTo('create-feature');
    }
  }
});
