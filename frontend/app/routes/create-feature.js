import Ember from 'ember';

export default Ember.Route.extend({
  notify: Ember.inject.service(),
  model() {
    return {};
  },
  actions: {
    save() {
      const notify = this.get('notify');

      const newFeature = this.store.createRecord('feature', this.currentModel);
      newFeature.save().then((feature) => {
        notify.success('Feature created');
        this.transitionTo('feature', feature.id);
      }).catch((reason) => {
        notify.error('Error: ' + reason);
      });
    },
    cancel() {
      this.transitionTo('features');
    }
  }
});
