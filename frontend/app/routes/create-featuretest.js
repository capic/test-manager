import Ember from 'ember';

export default Ember.Route.extend({
  notify: Ember.inject.service(),
  model(params) {
    let featuretest = this.store.createRecord('featuretest');

    return {featuretest: featuretest, featureId: params.feature_id};
  },
  actions: {
    save: function(featureId) {
      const notify = this.get('notify');
      const self = this;

      this.store.findRecord('feature', featureId).then((feature) => {
        this.currentModel.featuretest.set('feature', feature);

        this.currentModel.featuretest.save().then((featuretest) => {
          notify.success('Feature test created');
          self.transitionTo('feature', this.currentModel.featureId);
        }).catch((reason) => {
          notify.error('Error: ' + reason);
        });
      }).catch((reason) => {
        notify.error('Error: ' + reason);
      });
    },
    cancel: function() {
      // this.currentModel.featuretest.rollback();
      this.transitionTo('feature', this.currentModel.featureId);
    }
  }
});
