import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    const feature = this.store.findRecord('feature', params.feature_id)
    // const featuretests = [];//this.store.query('featuretest', {feature_id: params.feature_id});
    return {statuses: this.modelFor('application'), feature: feature/*, featuretests: featuretests*/};
  },
  actions: {
    create(feature) {
      this.transitionTo('create-featuretest', feature);
    }
  }
});
