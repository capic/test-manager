import { Model, belongsTo } from 'ember-cli-mirage';

export default Model.extend({
  feature: belongsTo('feature'),
  status: belongsTo('status')
});
