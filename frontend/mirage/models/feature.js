import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  featuretests: hasMany('featuretest')
});
