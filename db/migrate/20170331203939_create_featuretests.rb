class CreateFeaturetests < ActiveRecord::Migration[5.0]
  def change
    create_table :featuretests do |t|
      t.string :name
      t.references :status, foreign_key: true
      t.references :feature, foreign_key: true

      t.timestamps
    end
  end
end
