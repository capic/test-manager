status_undefined = Status.create(id: 1, name: 'undefined')
status_passed = Status.create(id: 2, name: 'passed')
status_failed = Status.create(id: 3, name: 'failed')

# feature1 =  Feature.create(name: 'feature 1')
# feature2 =  Feature.create(name: 'feature 2')
# feature3 =  Feature.create(name: 'feature 3')

# test1 =  Featuretest.create(name: 'feature 1 test 1', status: status_undefined, feature: feature1)
# test2 =  Featuretest.create(name: 'feature 1 test 2', status: status_undefined, feature: feature1)

# test3 =  Featuretest.create(name: 'feature 2 test 1', status: status_undefined, feature: feature2)
# test4 =  Featuretest.create(name: 'feature 2 test 2', status: status_undefined, feature: feature2)
# test5 =  Featuretest.create(name: 'feature 2 test 3', status: status_undefined, feature: feature2)
# test6 =  Featuretest.create(name: 'feature 2 test 4', status: status_undefined, feature: feature2)

# test7 =  Featuretest.create(name: 'feature 3 test 1', status: status_undefined, feature: feature3)