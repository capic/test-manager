class FeaturetestsController < ApplicationController
  before_action :set_featuretest, only: [:show, :update, :destroy]

  # GET /featuretests
  def index
    @featuretests = Featuretest.all.order(:name)

    render json: @featuretests
  end

  # GET /featuretests/1
  def show
    render json: @featuretest
  end

  # POST /featuretests
  def create
    @featuretest = Featuretest.new(featuretest_params)
    @featuretest.status = Status.find(1)

    if @featuretest.save
      render json: @featuretest, status: :created, location: @featuretest
    else
      render json: @featuretest.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /featuretests/1
  def update
    if @featuretest.update(featuretest_params)
      render json: @featuretest
    else
      render json: @featuretest.errors, status: :unprocessable_entity
    end
  end

  # DELETE /featuretests/1
  def destroy
    @featuretest.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_featuretest
      @featuretest = Featuretest.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def featuretest_params
      # params.require(:featuretest).permit(:name, :status, :feature_id)
      ActiveModelSerializers::Deserialization.jsonapi_parse(params)
    end


end
