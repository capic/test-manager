class FeaturetestSerializer < ActiveModel::Serializer
  attributes :id, :name
  belongs_to :feature
  belongs_to :status
end
