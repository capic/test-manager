class StatusSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_many :featuretests
end
